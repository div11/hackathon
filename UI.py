# import openpyxl and tkinter modules 
from openpyxl import *
from tkinter import *
  
# globally declare wb and sheet variable 
  
# opening the existing excel file 
wb = load_workbook('D:\\Divyashree\\Hackathon\\excel.xlsx') 
  
# create the sheet object 
sheet = wb.active 
  
  
def excel(): 
      
    # resize the width of columns in 
    # excel spreadsheet 
    sheet.column_dimensions['A'].width = 20
    sheet.column_dimensions['B'].width = 10
    sheet.column_dimensions['C'].width = 10
    sheet.column_dimensions['D'].width = 10
    sheet.column_dimensions['E'].width = 10
    sheet.column_dimensions['F'].width = 10
    sheet.column_dimensions['G'].width = 10
    sheet.column_dimensions['H'].width = 20
    sheet.column_dimensions['I'].width = 20
    sheet.column_dimensions['J'].width = 30
    sheet.column_dimensions['K'].width = 30
    sheet.column_dimensions['L'].width = 30
    sheet.column_dimensions['M'].width = 30
    sheet.column_dimensions['N'].width = 30
    sheet.column_dimensions['O'].width = 30
    sheet.column_dimensions['P'].width = 30
    sheet.column_dimensions['Q'].width = 20
    sheet.column_dimensions['R'].width = 30
    sheet.column_dimensions['S'].width = 20
    sheet.column_dimensions['T'].width = 20
    sheet.column_dimensions['U'].width = 20
  
    # write given data to an excel spreadsheet 
    # at particular location 
    sheet.cell(row=1, column=1).value = "customerID"
    sheet.cell(row=1, column=2).value = "gender"
    sheet.cell(row=1, column=3).value = "SeniorCitizen"
    sheet.cell(row=1, column=4).value = "Partner"
    sheet.cell(row=1, column=5).value = "Dependents"
    sheet.cell(row=1, column=6).value = "tenure"
    sheet.cell(row=1, column=7).value = "PhoneService"
    sheet.cell(row=1, column=8).value = "MultipleLines"
    sheet.cell(row=1, column=9).value = "InternetService"
    sheet.cell(row=1, column=10).value = "OnlineSecurity"
    sheet.cell(row=1, column=11).value = "OnlineBackup"
    sheet.cell(row=1, column=12).value = "DeviceProtection"
    sheet.cell(row=1, column=13).value = "TechSupport"
    sheet.cell(row=1, column=14).value = "StreamingTV"
    sheet.cell(row=1, column=15).value = "StreamingMovies"
    sheet.cell(row=1, column=16).value = "Contract"
    sheet.cell(row=1, column=17).value = "PaperlessBilling"
    sheet.cell(row=1, column=18).value = "PaymentMethod"
    sheet.cell(row=1, column=19).value = "MonthlyCharges"
    sheet.cell(row=1, column=20).value = "TotalCharges"
    sheet.cell(row=1, column=21).value = "Churn"
  
  
# Function to set focus (cursor) 
def focus1(event): 
    # set focus on the course_field box 
    customerID_field.focus_set() 
  
  
# Function to set focus 
def focus2(event): 
    # set focus on the sem_field box 
    gender_field.focus_set() 
  
  
# Function to set focus 
def focus3(event): 
    # set focus on the form_no_field box 
    SeniorCitizen_field.focus_set() 
  
  
# Function to set focus 
def focus4(event): 
    # set focus on the contact_no_field box 
    Partner_field.focus_set() 
  
  
# Function to set focus 
def focus5(event): 
    # set focus on the email_id_field box 
    Dependents_field.focus_set() 
  
  
# Function to set focus 
def focus6(event): 
    # set focus on the address_field box 
    tenure_field.focus_set()

# Function to set focus 
def focus7(event): 
    # set focus on the address_field box 
    PhoneService_field.focus_set()

# Function to set focus 
def focus8(event): 
    # set focus on the address_field box 
    MultipleLines_field.focus_set()

# Function to set focus 
def focus9(event): 
    # set focus on the address_field box 
    InternetService_field.focus_set()

# Function to set focus 
def focus10(event): 
    # set focus on the address_field box 
    OnlineSecurity_field.focus_set()

# Function to set focus 
def focus11(event): 
    # set focus on the address_field box 
    OnlineBackup_field.focus_set()

# Function to set focus 
def focus12(event): 
    # set focus on the address_field box 
    DeviceProtection_field.focus_set()

# Function to set focus 
def focus13(event): 
    # set focus on the address_field box 
    TechSupport_field.focus_set()

# Function to set focus 
def focus14(event): 
    # set focus on the address_field box 
    StreamingTV_field.focus_set()

# Function to set focus 
def focus15(event): 
    # set focus on the address_field box 
    StreamingMovies_field.focus_set()

# Function to set focus 
def focus16(event): 
    # set focus on the address_field box 
    Contract_field.focus_set()

# Function to set focus 
def focus17(event): 
    # set focus on the address_field box 
    PaperlessBilling_field.focus_set()

# Function to set focus 
def focus18(event): 
    # set focus on the address_field box 
    PaymentMethod_field.focus_set()

# Function to set focus 
def focus19(event): 
    # set focus on the address_field box 
    MonthlyCharges_field.focus_set()

# Function to set focus 
def focus20(event): 
    # set focus on the address_field box 
    TotalCharges_field.focus_set()
'''
# Function to set focus 
def focus21(event): 
    # set focus on the address_field box 
    Churn_field.focus_set()
''' 
  
# Function for clearing the 
# contents of text entry boxes 
def clear(): 
      
    # clear the content of text entry box 
    customerID_field.delete(0, END) 
    gender_field.delete(0, END) 
    SeniorCitizen_field.delete(0, END) 
    Partner_field.delete(0, END) 
    Dependents_field.delete(0, END) 
    tenure_field.delete(0, END) 
    PhoneService_field.delete(0, END)
    MultipleLines_field.delete(0, END)
    InternetService_field.delete(0, END)
    OnlineSecurity_field.delete(0, END)
    OnlineBackup_field.delete(0, END)
    DeviceProtection_field.delete(0, END)
    TechSupport_field.delete(0, END)
    StreamingTV_field.delete(0, END)
    StreamingMovies_field.delete(0, END)
    Contract_field.delete(0, END)
    PaperlessBilling_field.delete(0, END)
    PaymentMethod_field.delete(0, END)
    MonthlyCharges_field.delete(0, END)
    TotalCharges_field.delete(0, END)
    #Churn_field.delete(0, END)
  
  
# Function to take data from GUI  
# window and write to an excel file 
def insert(): 
      
    # if user not fill any entry 
    # then print "empty input" 
    if (customerID_field.get() == "" and
        gender_field.get() == "" and
        SeniorCitizen_field.get() == "" and
        Partner_field.get() == "" and
        Dependents_field.get() == "" and
        tenure_field.get() == "" and
        PhoneService_field.get() == "" and
        MultipleLines_field.get() == "" and
        InternetService_field.get() == "" and
        OnlineSecurity_field.get() == "" and
        OnlineBackup_field.get() == "" and
        DeviceProtection_field.get() == "" and
        TechSupport_field.get() == "" and
        StreamingTV_field.get() == "" and
        StreamingMovies_field.get() == "" and
        Contract_field.get() == "" and
        PaperlessBilling_field.get() == "" and
        PaymentMethod_field.get() == "" and
        MonthlyCharges_field.get() == "" and
        TotalCharges_field.get() == ""):# and
        #Churn_field.get() == ""): 
              
        print("empty input") 
  
    else: 
  
        # assigning the max row and max column 
        # value upto which data is written 
        # in an excel sheet to the variable 
        current_row = sheet.max_row 
        current_column = sheet.max_column 
  
        # get method returns current text 
        # as string which we write into 
        # excel spreadsheet at particular location 
        sheet.cell(row=current_row + 1, column=1).value = customerID_field.get() 
        sheet.cell(row=current_row + 1, column=2).value = gender_field.get() 
        sheet.cell(row=current_row + 1, column=3).value = seniorCitizen.get() 
        sheet.cell(row=current_row + 1, column=4).value = partner.get() 
        sheet.cell(row=current_row + 1, column=5).value = dependents.get() 
        sheet.cell(row=current_row + 1, column=6).value = tenure_field.get() 
        sheet.cell(row=current_row + 1, column=7).value = phoneService.get()
        sheet.cell(row=current_row + 1, column=8).value = multipleLines.get()
        sheet.cell(row=current_row + 1, column=9).value = internetService.get()
        sheet.cell(row=current_row + 1, column=10).value = onlineSecurity.get()
        sheet.cell(row=current_row + 1, column=11).value = onlineBackup.get()
        sheet.cell(row=current_row + 1, column=12).value = deviceProtection.get()
        sheet.cell(row=current_row + 1, column=13).value = techSupport.get()
        sheet.cell(row=current_row + 1, column=14).value = streamingTV.get()
        sheet.cell(row=current_row + 1, column=15).value = streamingMovies.get()
        sheet.cell(row=current_row + 1, column=16).value = Contract_field.get()
        sheet.cell(row=current_row + 1, column=17).value = paperlessBilling.get()
        sheet.cell(row=current_row + 1, column=18).value = PaymentMethod_field.get()
        sheet.cell(row=current_row + 1, column=19).value = MonthlyCharges_field.get()
        sheet.cell(row=current_row + 1, column=20).value = TotalCharges_field.get()
        #sheet.cell(row=current_row + 1, column=21).value = Churn_field.get()
  
        # save the file 
        wb.save('D:\\Divyashree\\Hackathon\\excel.xlsx') 
  
        # set focus on the customerID_field box 
        customerID_field.focus_set() 
  
        # call the clear() function 
        clear() 


  
  
# Driver code 
if __name__ == "__main__": 
      
    # create a GUI window 
    root = Tk() 
  
    # set the background colour of GUI window 
    root.configure(background='light blue' ) 
  
    # set the title of GUI window 
    root.title("Churn Prediction") 
  
    # set the configuration of GUI window 
    root.geometry("700x700") 
  
    excel() 
  
    # create a Form label 
    heading = Label(root, text="Churn Prediction", bg="light blue", font=("Helvetica", 20)) 
  
    # create a Name label 
    customerID = Label(root, text="CustomerID", bg="light blue", font=("Helvetica", 12)) 
  
    # create a Course label 
    gender = Label(root, text="Gender", bg="light blue", font=("Helvetica", 12)) 
  
    # create a Semester label 
    SeniorCitizen = Label(root, text="SeniorCitizen", bg="light blue", font=("Helvetica", 12))
    seniorCitizen = StringVar()
    SeniorCitizenY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=seniorCitizen, value="Yes",bg="light blue")
    SeniorCitizenN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=seniorCitizen, value="No", bg="light blue")
    
    # create a Form No. lable 
    Partner = Label(root, text="Partner", bg="light blue", font=("Helvetica", 12)) 
    partner = StringVar()
    PartnerY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=partner, value="Yes",bg="light blue")
    PartnerN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=partner, value="No", bg="light blue")
    
  
    # create a Contact No. label 
    Dependents = Label(root, text="Dependents", bg="light blue", font=("Helvetica", 12)) 
    dependents = StringVar()
    DependentsY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=dependents, value="Yes",bg="light blue")
    DependentsN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=dependents, value="No", bg="light blue")
    
  
    # create a Email id label 
    tenure = Label(root, text="Tenure", bg="light blue", font=("Helvetica", 12))
  
    # create a address label 
    PhoneService = Label(root, text="PhoneService", bg="light blue", font=("Helvetica", 12)) 
    phoneService = StringVar()
    PhoneServiceY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=phoneService, value="Yes",bg="light blue")
    PhoneServiceN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=phoneService, value="No", bg="light blue")
    

    # create a address label 
    MultipleLines = Label(root, text="MultipleLines", bg="light blue", font=("Helvetica", 12))
    multipleLines = StringVar()
    MultipleLinesY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=multipleLines, value="Yes",bg="light blue")
    MultipleLinesN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=multipleLines, value="No", bg="light blue")
    

    # create a address label 
    InternetService = Label(root, text="InternetService", bg="light blue", font=("Helvetica", 12))
    internetService = StringVar()
    InternetServiceY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=internetService, value="Yes",bg="light blue")
    InternetServiceN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=internetService, value="No", bg="light blue")
    

    # create a address label 
    OnlineSecurity = Label(root, text="OnlineSecurity", bg="light blue", font=("Helvetica", 12))
    onlineSecurity = StringVar()
    OnlineSecurityY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=onlineSecurity, value="Yes",bg="light blue")
    OnlineSecurityN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=onlineSecurity, value="No", bg="light blue")
    

    # create a address label 
    OnlineBackup = Label(root, text="OnlineBackup", bg="light blue", font=("Helvetica", 12))
    onlineBackup = StringVar()
    OnlineBackupY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=onlineBackup, value="Yes",bg="light blue")
    OnlineBackupN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=onlineBackup, value="No", bg="light blue")
    

    # create a address label 
    DeviceProtection = Label(root, text="DeviceProtection", bg="light blue", font=("Helvetica", 12))
    deviceProtection = StringVar()
    DeviceProtectionY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=deviceProtection, value="Yes",bg="light blue")
    DeviceProtectionN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=deviceProtection, value="No", bg="light blue")
    

    # create a address label 
    TechSupport = Label(root, text="TechSupport", bg="light blue", font=("Helvetica", 12))
    techSupport = StringVar()
    TechSupportY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=techSupport, value="Yes",bg="light blue")
    TechSupportN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=techSupport, value="No", bg="light blue")
    

    # create a address label 
    StreamingTV = Label(root, text="StreamingTV", bg="light blue", font=("Helvetica", 12))
    streamingTV = StringVar()
    StreamingTVY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=streamingTV, value="Yes",bg="light blue")
    StreamingTVN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=streamingTV, value="No", bg="light blue")
    

    # create a address label 
    StreamingMovies = Label(root, text="StreamingMovies", bg="light blue", font=("Helvetica", 12))
    streamingMovies = StringVar()
    StreamingMoviesY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=streamingMovies, value="Yes",bg="light blue")
    StreamingMoviesN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=streamingMovies, value="No", bg="light blue")
    

    # create a address label 
    Contract = Label(root, text="Contract", bg="light blue", font=("Helvetica", 12))

    # create a address label 
    PaperlessBilling = Label(root, text="PaperlessBilling", bg="light blue", font=("Helvetica", 12))
    paperlessBilling = StringVar()
    PaperlessBillingY = Radiobutton(root, text="Yes", padx = 10, indicatoron = 0, variable=paperlessBilling, value="Yes",bg="light blue")
    PaperlessBillingN = Radiobutton(root, text="No", padx = 10, indicatoron = 0, variable=paperlessBilling, value="No", bg="light blue")
    

    # create a address label 
    PaymentMethod = Label(root, text="PaymentMethod", bg="light blue", font=("Helvetica", 12))

    # create a address label 
    MonthlyCharges = Label(root, text="MonthlyCharges", bg="light blue", font=("Helvetica", 12))

    # create a address label 
    TotalCharges = Label(root, text="TotalCharges", bg="light blue", font=("Helvetica", 12))
    
    
    

    # create a address label 
    #Churn = Label(root, text="Churn", bg="light blue") 
    
    # grid method is used for placing 
    # the widgets at respective positions 
    # in table like structure . 
    heading.grid(row=0, column=1,columnspan=2) 
    customerID.grid(row=1, column=0) 
    gender.grid(row=2, column=0) 
    SeniorCitizen.grid(row=3, column=0)
    SeniorCitizenY.grid(row=3, column=1)
    SeniorCitizenN.grid(row=3, column=2)
    Partner.grid(row=4, column=0) 
    PartnerY.grid(row=4, column=1) 
    PartnerN.grid(row=4, column=2) 
    Dependents.grid(row=5, column=0)
    DependentsY.grid(row=5, column=1)
    DependentsN.grid(row=5, column=2)
    tenure.grid(row=6, column=0)  
    PhoneService.grid(row=7, column=0)
    PhoneServiceY.grid(row=7, column=1)
    PhoneServiceN.grid(row=7, column=2)
    MultipleLines.grid(row=8, column=0)
    MultipleLinesY.grid(row=8, column=1)
    MultipleLinesN.grid(row=8, column=2)
    InternetService.grid(row=9, column=0)
    InternetServiceY.grid(row=9, column=1)
    InternetServiceN.grid(row=9, column=2)
    OnlineSecurity.grid(row=10, column=0)
    OnlineSecurityY.grid(row=10, column=1)
    OnlineSecurityN.grid(row=10, column=2)
    OnlineBackup.grid(row=11, column=0)
    OnlineBackupY.grid(row=11, column=1)
    OnlineBackupN.grid(row=11, column=2)
    DeviceProtection.grid(row=12, column=0)
    DeviceProtectionY.grid(row=12, column=1)
    DeviceProtectionN.grid(row=12, column=2)
    TechSupport.grid(row=13, column=0)
    TechSupportY.grid(row=13, column=1)
    TechSupportN.grid(row=13, column=2)
    StreamingTV.grid(row=14, column=0)
    StreamingTVY.grid(row=14, column=1)
    StreamingTVN.grid(row=14, column=2)
    StreamingMovies.grid(row=15, column=0)
    StreamingMoviesY.grid(row=15, column=1)
    StreamingMoviesN.grid(row=15, column=2)
    Contract.grid(row=16, column=0)
    PaperlessBilling.grid(row=17, column=0)
    PaperlessBillingY.grid(row=17, column=1)
    PaperlessBillingN.grid(row=17, column=2)
    PaymentMethod.grid(row=18, column=0)
    MonthlyCharges.grid(row=19, column=0)
    TotalCharges.grid(row=20, column=0)
    #Churn.grid(row=21, column=0) 
  
    # create a text entry box 
    # for typing the information 
    customerID_field = Entry(root) 
    gender_field = Entry(root) 
    SeniorCitizen_field = Entry(root) 
    Partner_field = Entry(root) 
    Dependents_field = Entry(root) 
    tenure_field = Entry(root) 
    PhoneService_field = Entry(root)
    MultipleLines_field = Entry(root)
    InternetService_field = Entry(root)
    OnlineSecurity_field = Entry(root)
    OnlineBackup_field = Entry(root)
    DeviceProtection_field = Entry(root)
    TechSupport_field = Entry(root)
    StreamingTV_field = Entry(root)
    StreamingMovies_field = Entry(root)
    Contract_field = Entry(root)
    PaperlessBilling_field = Entry(root)
    PaymentMethod_field = Entry(root)
    MonthlyCharges_field = Entry(root)
    TotalCharges_field = Entry(root)
    #Churn_field = Entry(root)

  
    # bind method of widget is used for 
    # the binding the function with the events 
  
    # whenever the enter key is pressed 
    # then call the focus1 function 
    customerID_field.bind("<Return>", focus1) 
  
    # whenever the enter key is pressed 
    # then call the focus2 function 
    gender_field.bind("<Return>", focus2) 
  
    # whenever the enter key is pressed 
    # then call the focus3 function 
    SeniorCitizen_field.bind("<Return>", focus3) 
  
    # whenever the enter key is pressed 
    # then call the focus4 function 
    Partner_field.bind("<Return>", focus4) 
  
    # whenever the enter key is pressed 
    # then call the focus5 function 
    Dependents_field.bind("<Return>", focus5) 
  
    # whenever the enter key is pressed 
    # then call the focus6 function 
    tenure_field.bind("<Return>", focus6)

    # whenever the enter key is pressed 
    # then call the focus7 function 
    PhoneService_field.bind("<Return>", focus7)

    # whenever the enter key is pressed 
    # then call the focus8 function 
    MultipleLines_field.bind("<Return>", focus8)

    # whenever the enter key is pressed 
    # then call the focus9 function 
    InternetService_field.bind("<Return>", focus9)

    # whenever the enter key is pressed 
    # then call the focus10 function 
    OnlineSecurity_field.bind("<Return>", focus10)

    # whenever the enter key is pressed 
    # then call the focus11 function 
    OnlineBackup_field.bind("<Return>", focus11)

    # whenever the enter key is pressed 
    # then call the focus12 function 
    DeviceProtection_field.bind("<Return>", focus12)

    # whenever the enter key is pressed 
    # then call the focus13 function 
    TechSupport_field.bind("<Return>", focus13)

    # whenever the enter key is pressed 
    # then call the focus14 function 
    StreamingTV_field.bind("<Return>", focus14)

    # whenever the enter key is pressed 
    # then call the focus15 function 
    StreamingMovies_field.bind("<Return>", focus15)

    # whenever the enter key is pressed 
    # then call the focus16 function 
    Contract_field.bind("<Return>", focus16)

    # whenever the enter key is pressed 
    # then call the focus17 function 
    PaperlessBilling_field.bind("<Return>", focus17)

    # whenever the enter key is pressed 
    # then call the focus18 function 
    PaymentMethod_field.bind("<Return>", focus18)

    # whenever the enter key is pressed 
    # then call the focus19 function 
    MonthlyCharges_field.bind("<Return>", focus19)

    # whenever the enter key is pressed 
    # then call the focus20 function 
    TotalCharges_field.bind("<Return>", focus20)

    # whenever the enter key is pressed 
    # then call the focus21 function 
    #Churn_field.bind("<Return>", focus21)
  
    # grid method is used for placing 
    # the widgets at respective positions 
    # in table like structure . 
    customerID_field.grid(row=1, column=1, ipadx="100", columnspan=2, sticky=W+E+N+S, padx=10, pady=10) 
    gender_field.grid(row=2, column=1, ipadx="200", columnspan=2, sticky=W+E+N+S, padx=10, pady=10) 
    #SeniorCitizen_field.grid(row=3, column=1, ipadx="0") 
    #Partner_field.grid(row=4, column=1, ipadx="100")  
    #Dependents_field.grid(row=5, column=1, ipadx="100") 
    tenure_field.grid(row=6, column=1,  ipadx="200", columnspan=2, sticky=W+E+N+S, padx=10, pady=10)
    #PhoneService_field.grid(row=7, column=1, ipadx="100")
    #MultipleLines_field.grid(row=8, column=1, ipadx="100")
    #InternetService_field.grid(row=9, column=1, ipadx="100")
    #OnlineSecurity_field.grid(row=10, column=1, ipadx="100")
    #OnlineBackup_field.grid(row=11, column=1, ipadx="100")
    #DeviceProtection_field.grid(row=12, column=1, ipadx="100")
    #TechSupport_field.grid(row=13, column=1, ipadx="100")
    #StreamingTV_field.grid(row=14, column=1, ipadx="100")
    #StreamingMovies_field.grid(row=15, column=1, ipadx="100")
    Contract_field.grid(row=16, column=1,  ipadx="200", columnspan=2, sticky=W+E+N+S, padx=10, pady=10)
    #PaperlessBilling_field.grid(row=17, column=1, ipadx="100")
    PaymentMethod_field.grid(row=18, column=1, ipadx="200", columnspan=2, sticky=W+E+N+S, padx=10, pady=10)
    MonthlyCharges_field.grid(row=19, column=1,  ipadx="200", columnspan=2, sticky=W+E+N+S, padx=10, pady=10)
    TotalCharges_field.grid(row=20, column=1, ipadx="200", columnspan=2, sticky=W+E+N+S, padx=10, pady=10)
    #Churn_field.grid(row=21, column=1, ipadx="100")

  
    # call excel function 
    excel() 
  
    # create a Submit Button and place into the root window 
    submit = Button(root, text="Submit", fg="Black", 
                            bg="Red", command=insert) 
    submit.grid(row=22, column=1, columnspan=2) 
  
    # start the GUI 
    root.mainloop() 


