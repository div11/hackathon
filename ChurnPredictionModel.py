
def Preprocessing(df):
    
    #converting TotalCharges which is object to float
    #checking if it has any empty strings
    '''
    for i in df['TotalCharges']:
        if(i==' '):
            print("True")
    '''
    #converting empty string to numeric value of 0
    df['TotalCharges'] = df['TotalCharges'].replace(" ", 0).astype('float32')
    #converting obj to float
    pd.to_numeric(df['TotalCharges'])


    #converting Yes/No categorial data into numeric data
    for col in df.columns:
        if col in ('Partner', 'Dependents','PhoneService','MultipleLines','OnlineSecurity','OnlineBackup','DeviceProtection','TechSupport','StreamingTV','StreamingMovies','PaperlessBilling','Churn'):
            df[col]=df[col].map({'Yes':1,'No':0, 'No internet service':0, 'No phone service':0})


    #converting remaining categorial data to numeric
    for col in df.columns:
        if type(col) =='object':
            df = pd.get_dummies(df, columns=[col])


    #converting gender object to category
    df["gender"] = df["gender"].astype('category')



    #converting 'gender' categorial data to numeric 'gender_code'
    df["gender"] = df["gender"].cat.codes


    #Converting remaining categorial data to numeric
    for i in("InternetService","Contract","PaymentMethod"):
        df[i] = df[i].astype('category')
        df[i] = df[i].cat.codes


    #deleting customerID as we have numeric serial no
    del df['customerID']

    df.head()
    return df
        
        
        

import pandas as pd
df=pd.read_csv('D:\\Divyashree\\Hackathon\\WA_Fn-UseC_-Telco-Customer-Churn.csv')
df=Preprocessing(df)
#using random forest classifier on train data
from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(n_estimators=50)
features = df.drop(["Churn"], axis=1).columns
clf.fit(df[features], df["Churn"])


data_xls = pd.read_excel('D:\\Divyashree\\Hackathon\\excel.xlsx', 'Sheet1', index_col=None)
data_xls.to_csv('D:\\Divyashree\\Hackathon\\testData.csv', encoding='utf-8', index=False)

df_test=pd.read_csv('D:\\Divyashree\\Hackathon\\testData.csv')
df_test=Preprocessing(df_test)
features = df_test.drop(["Churn"], axis=1).columns

predictions = clf.predict(df_test[features])
probs = clf.predict_proba(df_test[features])
print(predictions)