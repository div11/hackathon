
# coding: utf-8

# In[1]:


import pandas as pd
df=pd.read_csv('D:\\Divyashree\\Hackathon\\WA_Fn-UseC_-Telco-Customer-Churn.csv')
df.head()


# In[2]:


#checking if any null values exists
df.isnull().any()


# In[3]:


#checking the data types
df.dtypes


# In[3]:


#converting TotalCharges which is object to float
#checking if it has any empty strings
for i in df['TotalCharges']:
    if(i==' '):
        print("True")
#converting empty string to numeric value of 0
df['TotalCharges'] = df['TotalCharges'].replace(" ", 0).astype('float32')
#converting obj to float
pd.to_numeric(df['TotalCharges'])


# In[4]:


#converting Yes/No categorial data into numeric data
for col in df.columns:
    if col in ('Partner', 'Dependents','PhoneService','MultipleLines','OnlineSecurity','OnlineBackup','DeviceProtection','TechSupport','StreamingTV','StreamingMovies','PaperlessBilling','Churn'):
        df[col]=df[col].map({'Yes':1,'No':0, 'No internet service':0, 'No phone service':0})


# In[5]:


df.head()
df.dtypes


# In[6]:


#converting remaining categorial data to numeric
for col in df.columns:
    if type(col) =='object':
        df = pd.get_dummies(df, columns=[col])


# In[7]:


df.head()


# In[8]:


df.dtypes


# In[9]:


#converting gender object to category
df["gender"] = df["gender"].astype('category')


# In[10]:


#converting 'gender' categorial data to numeric 'gender_code'
df["gender"] = df["gender"].cat.codes


# In[11]:


df.head()


# In[12]:


#Converting remaining categorial data to numeric
for i in("InternetService","Contract","PaymentMethod"):
    df[i] = df[i].astype('category')
    df[i] = df[i].cat.codes


# In[13]:


df.head()


# In[14]:


#deleting customerID as we have numeric serial no
del df['customerID']


# In[15]:


df.head()


# In[16]:


# dividing the data into test and train
from sklearn.model_selection import train_test_split
df_train, df_test = train_test_split(df, test_size=0.25)


# In[18]:


#using random forest classifier on train data
from sklearn.ensemble import RandomForestClassifier
clf = RandomForestClassifier(n_estimators=50)
features = df.drop(["Churn"], axis=1).columns
clf.fit(df_train[features], df_train["Churn"])


# In[19]:


#make predictions
predictions = clf.predict(df_test[features])
probs = clf.predict_proba(df_test[features])
print(predictions)


# In[20]:


print(probs)


# In[21]:


#printing accuracy
score = clf.score(df_test[features], df_test["Churn"])
print("Accuracy: ", score)


# In[22]:


#confusion matrix
from sklearn.metrics import confusion_matrix
#get_ipython().magic('matplotlib inline')
confusion_matrix = pd.DataFrame(
    confusion_matrix(df_test["Churn"], predictions), 
    columns=["Predicted False", "Predicted True"], 
    index=["Actual False", "Actual True"]
)
print(confusion_matrix)


# In[38]:





# In[49]:


#feature analysis
import matplotlib.pyplot as plt
import numpy as np
fig = plt.figure(figsize=(20, 18))
ax = fig.add_subplot(111)

df_f = pd.DataFrame(clf.feature_importances_, columns=["importance"])
df_f["labels"] = features
df_f.sort_values("importance", inplace=True, ascending=False)
print(df_f.head(5))

index = np.arange(len(clf.feature_importances_))
bar_width = 0.5
rects = plt.barh(index , df_f["importance"], bar_width, alpha=0.4, color='b', label='Main')
plt.yticks(index, df_f["labels"])
plt.show()


# In[47]:


df_test["prob_true"] = probs[:, 1]
df_risky = df_test[df_test["prob_true"] > 0.9]
print(df_risky.head(5)[["prob_true"]])


# In[48]:


df.head()

